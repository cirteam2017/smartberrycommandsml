/**
 * Created by andreacorda on 08/12/17.
 */
var express = require('express');
var bodyParser = require('body-parser');
var http = require('http');
var Handler = require('../handlers/handler.js')

var handler = new Handler();
const PORT = 10010;

var RoutesManager = function () {
	var app = express();
	app.use(bodyParser.json());
	var router = express.Router();
	app.use(router);

	app.put('/classify',handler.handleClassificationRequest)

	this.httpServer = http.createServer(app);
	this.startServer = startServer;
};

var startServer = function(){

	this.httpServer.listen(PORT);

	console.log('Server running on port '+PORT);
}

module.exports = RoutesManager;