/**
 * Created by andreacorda on 08/12/17.
 */

var Classifier = require('../neural/Classifier.js');
var classifier = null;
var Handler = function () {

	classifier = new Classifier();
	this.handleClassificationRequest = handleClassificationRequest;

};

var handleClassificationRequest = function (req,res) {
	var sentence = req.body.sentence;
	res.status(200).send(classifier.classify(sentence))
};

module.exports = Handler;