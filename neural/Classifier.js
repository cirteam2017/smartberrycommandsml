var limdu = require('limdu');
var serialize = require('serialization');
fs = require('fs')

// Now define our feature extractor - a function that takes a sample and adds features to a given features set:

var Classifier = function Classifier(){

	function newClassifierFunction() {
		var limdu = require('limdu');
		var TextClassifier = limdu.classifiers.multilabel.BinaryRelevance.bind(0, {
			binaryClassifierType: limdu.classifiers.Winnow.bind(0, {retrain_count: 10})
		});
		var WordExtractor = function(input, features) {
			input.split(" ").forEach(function(word) {
				features[word.toLowerCase()]=1;
			});
		};
		return new limdu.classifiers.EnhancedClassifier({
			classifierType: TextClassifier,
			featureExtractor: WordExtractor
		});
	}

	// Initialize a classifier with the base classifier type and the feature extractor:
	if (fs.existsSync("classifier.string")) {

		var content = fs.readFileSync('classifier.string', 'utf8');
		this.intentClassifier = serialize.fromString(content, __dirname);
	} else{
		this.intentClassifier = newClassifierFunction();

	// Train and test:
		this.intentClassifier.trainBatch([
			{input: "Turn on the light", output: "TURN_ON_LIGHT"},
			{input: "Turn off the light", output: "TURN_OFF_LIGHT"},
			{input: "Can you turn off the light", output: "TURN_OFF_LIGHT"},
			{input: "Can you turn on the light", output: "TURN_ON_LIGHT"},
			{input: "Water the plant", output: "WATER_PLANT"},
			{input: "What time is it", output:  "READ_TIME"},
			{input: "Open the blinder", output:  "OPEN_BLINDERS"},
			{input: "Open the blinder", output:  "OPEN_BLINDERS"},
			{input: "Open the courtain", output:  "OPEN_BLINDERS"},
			{input: "Open the courtain", output:  "OPEN_BLINDERS"},
			{input: "Close the blinder", output:  "CLOSE_BLINDERS"},
			{input: "Close the blinder", output:  "CLOSE_BLINDERS"},
			{input: "Close the courtain", output:  "CLOSE_BLINDERS"},
			{input: "Close the courtain", output:  "CLOSE_BLINDERS"}
		]);

		// Serialize the classifier (convert it to a string)
		var intentClassifierString = serialize.toString(this.intentClassifier, newClassifierFunction);
		fs.writeFile("classifier.string", intentClassifierString, function(err) {
			if(err) {
				return console.log(err);
			}
			console.log("The file was saved!");
		});
	}

	this.classify = classify;
};

var classify = function (sentence) {
	return this.intentClassifier.classify(sentence);
}

module.exports = Classifier;